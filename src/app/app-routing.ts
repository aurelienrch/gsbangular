import { Routes, RouterModule, PreloadAllModules } from '@angular/router';



import {ConnexionComponent} from './connexion/connexion.component';
import { MedecinsComponent } from './medecins/medecins.component';
import { VisitesComponent } from './visites/visites.component';
import { NavbarComponent } from './navbar/navbar.component';
import {AccueilComponent} from './accueil/accueil.component';

export const appRoutes: Routes = [
    { path: '', component: ConnexionComponent },
    { path: 'medecin', component: MedecinsComponent },
    { path: 'visite', component: VisitesComponent },
    { path: 'accueil', component: AccueilComponent },
];


export const AppRouting = RouterModule.forRoot(appRoutes, {
  //enableTracing: true,
  preloadingStrategy: PreloadAllModules
});