import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from "rxjs/internal/operators";
import { Observable } from "rxjs/Rx";
import { environment } from "../../environments/environment";

const apiRF_url = environment.apiRestFullUrl;

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }

  private extractData(res: Response) {
    let body = res;
    return body || {};
  }

  public chargerMedecins(numMedecin: number): Observable<any> {
    return this.http.get<any>(`${apiRF_url}` + numMedecin).pipe(map(this.extractData));
  }

}
