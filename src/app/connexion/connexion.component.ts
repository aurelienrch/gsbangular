import { Component, OnInit, } from '@angular/core';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms'
import { Router } from '@angular/router';

@Component({
  selector: 'app-connexion',
  templateUrl: './connexion.component.html',
  styleUrls: ['./connexion.component.css']
})
export class ConnexionComponent implements OnInit {
  public titre = "Connexion";
  constructor(private fb: FormBuilder, private router: Router) { }
  connexionForm: FormGroup
  public pwdIsGood = true;

  ngOnInit() {
    this.connexionForm = this.fb.group({
      login: new FormControl('', [Validators.required, Validators.minLength(1)]),
      password: new FormControl('', [Validators.required, Validators.minLength(1)]),
    });
  }

  onSubmit() {
    // TODO: Use EventEmitter with form value
    this.pwdIsGood = true;
    if (this.connexionForm.value.login == 'toto' && this.connexionForm.value.password == 'titi') {
      this.router.navigate(['accueil']);
    } else {
      this.pwdIsGood = false;
    }
  }

  formIsNotValid() {
    if (this.connexionForm.get('password').valid && this.connexionForm.get('login').valid) {
      return true;
    } else {
      return false;
    }
  }

}
