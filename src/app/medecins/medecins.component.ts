import { Component, OnInit } from '@angular/core';
import { ApiService } from "../services/api.service";
import { HttpClientModule } from '@angular/common/http';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-medecins',
  templateUrl: './medecins.component.html',
  styleUrls: ['./medecins.component.css']
})
export class MedecinsComponent implements OnInit {

  constructor(public api: ApiService) { }

  private medecinArray: Observable<any>;
  private mappedArray;

  ngOnInit() {
    this.getMedecin();
  }

  getMedecin() {
    let medecin = [];
    this.api.chargerMedecins(47).subscribe(data => {
      medecin = Object.values(data);
      this.medecinArray = data;
      this.mappedArray = Object.entries(medecin).map(([type, value]) => ({ type, value }));
      console.log(this.mappedArray);
    });
  }

}


